package br.com.cognizante.desafioMobile;

import org.openqa.selenium.support.PageFactory;

import junit.framework.Assert;
import mobile.func.FuncArquivo;

/**
 * Classe de funcionalidades
 * @author Ricardo Kohatsu
 *
 */
public class ConsultaPlacaPage extends ConsultaPlacaElementMap{

	public ConsultaPlacaPage(){
		PageFactory.initElements(TestRule.getDriver(), this);
	}
	
	public void aplicacaoIniciada() {
		placaLetras.isDisplayed();
	}

	public void preencherPlacaLetras(String valorPlacaLetras) {
		placaLetras.sendKeys(valorPlacaLetras);
	}
	public void preencherPlacaNumeros(String valorPlacaNumeros) {
		placaNumeros.sendKeys(valorPlacaNumeros);
	}
	public void apertarBotaoPesquisar() {
		botaoPesquisar.click();
	}

	public void validarPlacaValida() {
		String ufOrigem = campoUFOrigemBusca.getText();
		Assert.assertFalse(ufOrigem.toUpperCase().equals("PLACA INVÁLIDA"));
	}

	public void validarPlacaInvalida() {
		String ufOrigem = campoUFOrigemBusca.getText();
		Assert.assertTrue(ufOrigem.toUpperCase().equals("PLACA INVÁLIDA"));
	}	
	
	public void armazenarPlacaValida(){
		FuncArquivo funcArquivo = new FuncArquivo();
		String placa = campoPlacaBusca.getText();
		funcArquivo.registrarDados(placa);
	}

}
