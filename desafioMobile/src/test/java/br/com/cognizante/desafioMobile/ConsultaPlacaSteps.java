package br.com.cognizante.desafioMobile;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Classe de steps
 * @author Ricardo Kohatsu
 *
 */
public class ConsultaPlacaSteps {
    @Given ("o aplicativo iniciado")
    public void iniciarAplicacao(){
    	ConsultaPlacaPage consultaPlacaPage = new ConsultaPlacaPage();
    	consultaPlacaPage.aplicacaoIniciada();
    }
    @When ("^preencho o campo de letras da placa com valor \"(.*)\"$")
    public void preencherPlacaLetras(String placaLetras){
    	ConsultaPlacaPage consultaPlacaPage = new ConsultaPlacaPage();
    	consultaPlacaPage.preencherPlacaLetras(placaLetras);   
    }
    @And ("preencho o campo de numeros da placa com valor \"(.*)\"$")
    public void preencherPlacaNumeros(String placaNumeros){
    	ConsultaPlacaPage consultaPlacaPage = new ConsultaPlacaPage();
    	consultaPlacaPage.preencherPlacaNumeros(placaNumeros);   	
    }
    @And ("aperto o botao pesquisar")
    public void apertarBotaoPesquisar(){
    	ConsultaPlacaPage consultaPlacaPage = new ConsultaPlacaPage();
    	consultaPlacaPage.apertarBotaoPesquisar();    	
    }
    @Then ("na tela exibira o resultado da busca da placa Valida")
    public void validarResultadoBuscaPlacaValida(){
    	ConsultaPlacaPage consultaPlacaPage = new ConsultaPlacaPage();
    	consultaPlacaPage.validarPlacaValida();   	
    }
    @Then ("na tela exibira o resultado da busca da placa Invalida")
    public void validarResultadoBuscaPlacaInvalida(){
    	ConsultaPlacaPage consultaPlacaPage = new ConsultaPlacaPage();
    	consultaPlacaPage.validarPlacaValida();   	
    }    
    @And ("placa valida armazenada")
    public void armazenarPlaca(){
    	ConsultaPlacaPage consultaPlacaPage = new ConsultaPlacaPage();
    	consultaPlacaPage.armazenarPlacaValida();   	
    }
}
