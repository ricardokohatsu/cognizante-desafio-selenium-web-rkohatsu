package br.com.cognizante.desafioMobile;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Classe de mapeamento dos elementos do aplicativo
 * @author RM
 *
 */
public class ConsultaPlacaElementMap {
	
	@FindBy(id = "com.devplank.masterplaca:id/et_placa_letra")
	protected WebElement placaLetras;
	
	@FindBy(id = "com.devplank.masterplaca:id/et_placa_numero")
	protected WebElement placaNumeros;
	
	@FindBy(id = "com.devplank.masterplaca:id/fab_consultar")
	protected WebElement botaoPesquisar;
	
	@FindBy(id = "android:id/message")
	protected WebElement popUpAviso;
	
	@FindBy(id = "com.devplank.masterplaca:id/result_placa")
	protected WebElement campoPlacaBusca;	
	
	@FindBy(id = "com.devplank.masterplaca:id/result_uf_origem")
	protected WebElement campoUFOrigemBusca;		
}
