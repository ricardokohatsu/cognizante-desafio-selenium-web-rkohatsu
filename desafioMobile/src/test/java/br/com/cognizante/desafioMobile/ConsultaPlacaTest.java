package br.com.cognizante.desafioMobile;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/ConsultaPlaca.feature",
glue = { "" }, monochrome = true, dryRun = false)
public class ConsultaPlacaTest {

}
