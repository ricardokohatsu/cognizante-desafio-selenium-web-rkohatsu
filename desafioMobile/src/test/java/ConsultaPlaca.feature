@tag
Feature: Testes no aplicativo MasterPlaca

  @tag1
  Scenario: Consultar placa válida
    Given o aplicativo iniciado
    When preencho o campo de letras da placa com valor "FNZ"
    And preencho o campo de numeros da placa com valor "7145"
    And aperto o botao pesquisar
    Then na tela exibira o resultado da busca da placa Valida
    And placa valida armazenada

  @tag2
  Scenario: Consultar placa inválida
    Given o aplicativo iniciado
    When preencho o campo de letras da placa com valor "XXX"
    And preencho o campo de numeros da placa com valor "9999"
    And aperto o botao pesquisar
    Then na tela exibira o resultado da busca de placa Invalida
