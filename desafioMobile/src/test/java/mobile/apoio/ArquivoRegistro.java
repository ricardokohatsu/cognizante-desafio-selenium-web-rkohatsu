package mobile.apoio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Classe responsável por criar e armazenar dados em arquivo Txt
 * @author Ricardo Kohatsu
 *
 */
public class ArquivoRegistro{
	public void gerarArquivo(String diretorioArquivo,String nomeArquivo, String texto){
	    try {
	        File diretorio = new File(diretorioArquivo);
	        if (!diretorio.exists()) {
	        	diretorio.mkdirs();
	        }
	        File arquivo = new File(diretorio, nomeArquivo);
	        
	        BufferedWriter escritor = new BufferedWriter(new FileWriter(arquivo, true));
	        escritor.write(texto);
	        escritor.newLine();
	        escritor.flush();
	        escritor.close();
			

	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
}
