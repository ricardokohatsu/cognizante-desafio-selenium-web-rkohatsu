package mobile.func;

import mobile.apoio.ArquivoRegistro;

/**
 * Classe responsável por definir nome e local do arquivo Txt, e a armazenar os dados
 * @author RM
 *
 */
public class FuncArquivo {
	private ArquivoRegistro arquivo;
	public void registrarDados(String dados){
		arquivo = new ArquivoRegistro();
		String nomeArquivo = "registroPlacasValidas.txt";
		String diretorioArquivo = "C:\\Users\\Public\\RegistroPlaca";
		arquivo.gerarArquivo(diretorioArquivo, nomeArquivo, dados);
	}
}
