package apirest;

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import org.junit.Test;

public class TesteApi1 {

	public TesteApi1(){
		baseURI = "http://services.groupkt.com/country/get/all";
	}
	
	@Test
    public void testValidarQuantidadeRegistro() {
		
		//Variaveis para valida��o da resposta
		int qtdRegistros = 249;
		
         given()
         .when()
         	.get("/")
         .then()
         	.body("RestResponse.result.size()", is(qtdRegistros))
           .statusCode(200);    
    }

	@Test
    public void testeValidarPaisExiste() {
		
		//Variaveis para valida��o da resposta
        String nome = "Macao";
        String alpha2_coe = "MO";
		
         given()
         .when()
         	.get("/")
         .then()
         	.body("RestResponse.result.name", hasItem(nome))
         	.body("RestResponse.result.alpha2_code", hasItem(alpha2_coe))
           .statusCode(200);    
    }	
	
}
