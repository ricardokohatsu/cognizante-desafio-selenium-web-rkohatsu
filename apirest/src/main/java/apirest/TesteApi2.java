package apirest;

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import org.junit.Test;

public class TesteApi2 {

	public TesteApi2(){
		baseURI = "http://services.groupkt.com/state";
	}
	
	/**
	 * Teste para buscar registro por CountryCode
	 */
	@Test
    public void testeBuscaPorCountryCode() {
		
		//Variaveis para valida��o da resposta
        String coutryCode = "IND";
		
         given()
         .when()
         	.get("/get/" + coutryCode + "/all")
         .then()
         	.body("RestResponse.result.country", hasItem(coutryCode))
           .statusCode(200);    
    }
	
	/**
	 * Teste para buscar registro por CountryCode e Textual
	 */
	@Test
    public void testeBuscaTextual() {
		//Variaveis para valida��o da resposta
        String coutryCode = "IND";
        String texto = "Nagar";
		
         given()
         .when()
         	.get("/search/" + coutryCode + "?text=" + texto)
         .then()
         	.body("RestResponse.result.country", hasItem(coutryCode))
           .statusCode(200);    
    }	
	
}
