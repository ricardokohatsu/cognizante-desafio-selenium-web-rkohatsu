package selenium.apoio;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Navegador {

	private String nomeNavegador;
	
	public String getNomeNavegador() {
		return nomeNavegador;
	}

	public void setNomeNavegador(String nomeNavegador) {
		this.nomeNavegador = nomeNavegador;
	}

	public WebDriver configurarNavegador() {
	     switch (nomeNavegador.toUpperCase()) {  
	       case "CHROME":
	    	   ChromeOptions options= new ChromeOptions();
	    	   options.addArguments("--disable-extensions");
		   		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
				return new ChromeDriver(options);
	       default: 
	    	   System.out.println("Tipo de navegador n�o conhecido");  
	       	   return null;
	     }
	}
	
	public void acessarPaginaWeb(String url, WebDriver driver) {
		driver.get(url);
	}
	
}
