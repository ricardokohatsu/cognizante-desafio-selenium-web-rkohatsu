package selenium.telas;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TelaRegional{
	private WebDriver driver;
	
	public TelaRegional(WebDriver driver) {
		this.driver = driver;
	}

	public void preencherPesquisa(String texto) {
		driver.findElement(By.id("searchtext")).sendKeys(texto);
	}	
	
	public void clicarPesquisa() {
		driver.findElement(By.id("searchbutton")).click();
	}	
	
	public String capturarNomeProduto(int posicaoLista) {
		return driver.findElement(By.xpath("//a[@data-lurker_list_position='" + (posicaoLista - 1) + "']")).getAttribute("title").toString();
	}

	public String capturarValorProduto(int posicaoLista) {
		try {
			return driver.findElement(By.xpath("//a[@data-lurker_list_position='" + (posicaoLista - 1)+ "']//p[@class='OLXad-list-price']")).getText();
		}
		catch(NoSuchElementException ex){
			return "N�o h� valor";
		}
	}
	
	public boolean verificarPaginacao() {
		return driver.findElements(By.xpath("//a[@class='link' and @rel='next']")).size() > 0;
	}
	
	public void clicarPaginacao(int numPaginacao) {
		driver.findElement(By.xpath("//a[@class='link' and @title='" + numPaginacao + "']")).click();
	}	
	public void clicarProduto(int posicaoLista) {
		driver.findElement(By.xpath("//a[@data-lurker_list_position='" + (posicaoLista - 1) + "']")).click();
	}	
	
}
