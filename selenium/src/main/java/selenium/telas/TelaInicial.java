package selenium.telas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TelaInicial{
	private WebDriver driver;
	
	public TelaInicial(WebDriver driver) {
		this.driver = driver;
	}

	public void abrirTelaInicial() {
		driver.get("http://www.olx.com.br");
	}	
	
	public void selecionarEstado(String estado) {
		WebElement elementoEstado = driver.findElement(By.id("label-icon-state-" + estado.toLowerCase()));
		elementoEstado.click();
	}
}
