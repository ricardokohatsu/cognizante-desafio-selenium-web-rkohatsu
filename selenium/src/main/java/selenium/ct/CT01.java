package selenium.ct;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import selenium.apoio.Navegador;
import selenium.funcionalidade.FuncTelaInicial;
import selenium.funcionalidade.FuncTelaRegional;

/**
 * Desafio Selenium Web
 * 1 - Realizar uma pesquisa no site da OLX e imprimir SOMENTE titulo e valor 
 * dos CINCO primeiros resultados.
 * 
 * @author Ricardo Kohatsu - 05/10/2017
 *
 */
public class CT01 {
	
	WebDriver driver;
	
	@Before
	public void before() {
		
		Navegador navegador = new Navegador();
		navegador.setNomeNavegador("Chrome");
		driver = navegador.configurarNavegador();
		driver.manage().window().maximize();
	}
	
	@Test
	public void test() {
		
		FuncTelaInicial funcTelaInicial = new FuncTelaInicial(driver);
		FuncTelaRegional funcTelaRegional = new FuncTelaRegional(driver);

		funcTelaInicial.iniciarPaginaOlx();
		funcTelaInicial.acessarTelaPesquisaPorRegiao("SP");
		funcTelaRegional.realizarPesquisaProduto("carro");	
		funcTelaRegional.imprimirNomeValordoResultado(5);
		
	}
	
	@After
	public void after() {
		driver.close();
	}

}
