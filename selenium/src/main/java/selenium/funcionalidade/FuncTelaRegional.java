package selenium.funcionalidade;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import selenium.apoio.Navegador;
import selenium.telas.TelaRegional;

public class FuncTelaRegional {
	private Navegador navegador = new Navegador();
	private TelaRegional telaRegional;
	private FuncArquivo funcArquivo;
	private WebDriver driver;
	
	public FuncTelaRegional(WebDriver driver) {
		this.driver = driver;
		telaRegional = new TelaRegional(driver);
		funcArquivo = new FuncArquivo(driver);
	}
	
	public void realizarPesquisaProduto(String produto) {
		telaRegional.preencherPesquisa(produto);
		telaRegional.clicarPesquisa();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}		
	
	public void imprimirNomeValordoResultado(int quantidade) {
		String nomeProduto = "";
		String valorProduto = "";
		for(int cont = 1; cont <= quantidade; cont++) {
			nomeProduto = telaRegional.capturarNomeProduto(cont);
			valorProduto = telaRegional.capturarValorProduto(cont);
			System.out.println(cont + " - Nome: " + nomeProduto + " - Valor: " + valorProduto);
		}
	}
	
	public void validarPaginacao() {
		Assert.assertTrue("N�o foi encontrado pagina��o na tela", telaRegional.verificarPaginacao());
	}
	
	public void alterarPaginacao(int numPaginacao) {
		telaRegional.clicarPaginacao(numPaginacao);
	}
	
	public void clicarProdutoeEvidenciar(int posicaoLista){
		telaRegional.clicarProduto(posicaoLista);
		
		try {
			Thread.sleep(5000);
			funcArquivo.evidenciarTela();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}		
}
