package selenium.funcionalidade;

import org.openqa.selenium.WebDriver;
import selenium.apoio.Navegador;
import selenium.telas.TelaInicial;

public class FuncTelaInicial {
	
	private Navegador navegador = new Navegador();
	private TelaInicial telaInicial;
	private WebDriver driver;
	
	public FuncTelaInicial(WebDriver driver) {
		this.driver = driver;
		telaInicial = new TelaInicial(driver);
	}
	
	public void iniciarPaginaOlx() {
		String url = "http://www.olx.com.br";
		navegador.acessarPaginaWeb(url, driver);
	}
	
	public void acessarTelaPesquisaPorRegiao(String estado) {
		telaInicial.selecionarEstado(estado);
	}	
	

}
