package selenium.funcionalidade;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class FuncArquivo {
	
	private WebDriver driver;
	
	public FuncArquivo(WebDriver driver) {
		this.driver = driver;
	}	
	
	public void evidenciarTela() throws IOException {
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("C:\\Users\\Public\\evidencia.png"));
	}
}
